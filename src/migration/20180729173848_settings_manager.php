<?php
if(!class_exists("\jklz_settings_mgr\Migration\Migration")){
	include_once('Migration.php');
}

use \jklz_settings_mgr\Migration\Migration;

class SettingsManager extends Migration
{
    public function up() {
		//this will create all tables
		if (! $this->schema->hasTable('settingsmgr_settings')) {
			$this->schema->create('settingsmgr_settings', function(\Illuminate\Database\Schema\Blueprint $table){
				$table->increments('id');
				$table->string('key', 50)->unique();
				$table->string('type',20)->nullable();
				$table->text('value');//we will use text for json and encrypt for security
				$table->text('config');//we will use text for json and encrypt for security
				$table->text('history');//we will use text for json and encrypt for security
				$table->timestamps();
				
				/*
				now enter some example rows to settings
				*/
				
			});
		}
		//early builds used table name system_settings but we want to move to settingsmgr_settings and wasn't encrypted
		if ($this->schema->hasTable('system_settings')) {
			// where table exists we want to make sure it has the needed columns
			if( $this->schema->hasColumn('system_settings','key') && $this->schema->hasColumn('system_settings','type') && $this->schema->hasColumn('system_settings','value')){
				$oldsettings = \JKLZ\SettingsMgr\settings_old::get();
				if(count($oldsettings) > 0){
					/*
					if has settings we want to move to new db and encrypt
					*/
					foreach($oldsettings->toarray() AS $old){
						$check_new = \JKLZ\SettingsMgr\settingsdb::where('key',$old['key'])->first();
						if(!$check_new){
							//we only add if it doesn't alread exists
							$new = new \JKLZ\SettingsMgr\settingsdb();
							$new['key'] = $old['key'];
							$new['type'] = $old['type'];
							$new['value'] = $old['value'];
							$new['config'] = $old['config'];
							$new['history'] = $old['history'];
							$new->save();
							
						}
					}
				}
			}
		}
		
    }
    public function down() {
        $this->schema->drop('settingsmgr_settings');
    }
}
