<?php
namespace JKLZ\SettingsMgr;
//this will handle the db connecton

class settingsdb extends \JKLZ\Helper\DBModel {
	protected $table = "settingsmgr_settings";
	protected $fillable = array('key');
	protected $encryptable = ['history', 'config','value'];
	protected $jsonable = ['history', 'config'];
	
}
//
class settings_old extends \JKLZ\Helper\DBModel {
	protected $table = "system_settings";
	protected $fillable = array('key');
	protected $jsonable = ['history', 'config'];
	
}