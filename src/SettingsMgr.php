<?php
namespace JKLZ\SettingsMgr;

class Manager implements \ArrayAccess {
	
	protected static $_values = array();
	protected static $_fields = array();
	protected static $_errors = array();
	protected static $_hasloaded = false;
	private  static $_structure = array('key'=>NULL,'type'=>NULL,'config'=>array('max'=>'','min'=>'','maxlen'=>0,'minlen'=>0,'default'=>'','extra'=>array()),'history'=>array());
	private $_var_type = array('integer','int','string','str','bool','float');
	
    /**
     * Constructor.
     */
    public function __construct()
    {
		self::load();
    }

    /**
     * Destructor.
     */
    public function __destruct()
    {
		//destructor
		self::_save();
    }
	
	public function __toString(){
		self::load();
		return json_encode(self::$_values);
	}
	public function toArray(){
		self::load();
		return self::$_values;
	}
	public function debug(){
		self::load();
		$out = array(
			"_values" => self::$_values, 
			"_fields" => self::$_fields
		);
		return $out;
	}
	//this will handle setting variable
	private static function _handleSet($name, $value_in){
		self::load();
        $key_in = strtolower(trim($name));
		
		if(!array_key_exists($key_in, self::$_fields)){
			//doesnt exist
			self::$_errors[] = "ERROR: " . $key_in . " not available";
		}else{
			self::$_values[$key_in] = self::_processVariable($value_in, self::$_fields[$key_in]['type'], self::$_fields[$key_in]['config']);
		}
		self::_save();
	}
	//this will handle getting variable
	private static function _handleGet($name){
		self::load();
		$key_in = trim(strtolower($name));
		if(array_key_exists($key_in, self::$_fields)){
			return (array_key_exists($key_in, self::$_values) ? self::$_values[$key_in] : NULL);
		}
	}
	
	public function __call($name, $arguments){
		if(count($arguments) == 0){
			return self::_handleGet($name);
		}else{
			return self::_handleSet($name, $arguments[0]);
		}
		
		
	}
	public static function __callStatic($name, $arguments){
		if(count($arguments) == 0){
			return self::_handleGet($name);
		}else{
			return self::_handleSet($name, $arguments[0]);
		}
	}
	public function __invoke($name = NULL){
		self::load();
		if(is_null($name)){
			return self::$_values;
		}
		$key_in = trim(strtolower($name));
		if(array_key_exists($key_in, self::$_fields)){
			return (array_key_exists($key_in, self::$_values) ? self::$_values[$key_in] : NULL);
		}
		
	}
	public function __get($name){
		return self::_handleGet($name);
	}
	public function __set($name, $value)
    {
		return self::_handleSet($name, $value);
    }
	public function __debugInfo(){
		self::_load();
		return self::$_values;
	}
	public static function load(){
		if(count(self::$_values) == 0){
			self::_load();
		}
	}
	public static function reload(){
		//will handle reload
		self::$_hasloaded = false;
		self::_load();
	}
	private static function _load(){
		// actual process data from database only if needed.
		if(self::$_hasloaded){
			return true;
		}
		$allsettngs = \JKLZ\SettingsMgr\settingsdb::get()->toarray();
		//$loadsettings = self::$_fields;
		foreach($allsettngs AS $_item){
			self::$_fields[$_item['key']] = $_item;
		}
		foreach(self::$_fields AS $_k => $_data){
			self::$_values[$_k] = self::_processVariable($_data['value'],$_data['type'],$_data['config']);
		}
		//we only need to process the load once
		self::$_hasloaded = true;
		return self::$_values;
	}
	//write updated values back to database
	private static function _save(){
		foreach(self::$_values AS $_k => $_v){
			$field = self::$_fields[$_k];
			if($_v !== $field['value']){
				$update = \JKLZ\SettingsMgr\settingsdb::where('id',$field['id'])->first();
				$history = $update->history;
				$history[time()] = $_v;
				$update['history'] = $history;
				$update['value'] = self::_processVariable($_v,$field['type'],$field['config']);
				$update->save();
			}
		}
	}
	public function __isset($key=NULL){
		self::_load();
		return array_key_exists(trim(strtolower($key)), self::$_fields);
	}
	public function __unset($key=NULL){
		//will do nothing as we don't want to unset
	}
	public function AddVariable($variable="", $type=NULL, $cfg=array()){
		//this will handle adding new variable to db
		if(strlen($variable) < 3){
			return false;
		}
		//first we check variable doesnt already exist
		$newvar = \JKLZ\SettingsMgr\settingsdb::where('key',strtolower(trim($variable)))->first();
		if(!$newvar){
			//does not exist so we can add
			//get config ready
			$var_config = self::$_structure['config'];
			foreach($var_config AS $config_key => $config_value){
				//we cycle threw the default config for field to prevent unknown extra items
				if(array_key_exists($config_key, $cfg)){
					$var_config[$config_key] = $cfg[$config_key];
				}
			}
			//check type to verify we can use
			if(!in_array(strtolower(trim($type)), $this->_var_type)){
				//unsupported var type
				return false;
			}
			//now we should be good to save new variable
			$newvar = new \JKLZ\SettingsMgr\settingsdb();
			$newvar['key'] = strtolower(trim($variable));
			$newvar['type'] = strtolower(trim($type));
			$newvar['config'] = $var_config;
			$newvar['value'] = $var_config['default'];
			$newvar['history'] = [];
			$newvar->save();
			//now we reload 
			//\JKLZ\SettingsMgr\Manager::$variable($var_config['default']);
			
			//set value to default 
			\JKLZ\SettingsMgr\Manager::reload();
		}
	}
	private static function _processVariable($value = NULL, $type=NULL, $cfg=array()){
		
		switch (strtolower($type)) {
			case 'integer':
			case 'int':
				$value = (int)$value;
				break;
			case 'bool':
				$value = (bool)trim($value);
				break;
			case 'string':
			case 'str':
				$value = trim($value);
				$len = strlen($value);
				if($len > $cfg['maxlen'] && $cfg['maxlen'] > 0 && $cfg['maxlen'] >= $cfg['minlen']){
					substr( $value, 0, $cfg['maxlen']);
				}
				if($len < $cfg['minlen'] && $cfg['maxlen'] > 0 && $cfg['maxlen'] >= $cfg['minlen']){
					while(strlen($value) <= $cfg['minlen']){
						$value .= " ";
					}
					substr( $value, 0, $cfg['minlen']);
				}
				break;
		}
		return $value;
	}
	//allow settings to be used as array also
	public function offsetSet($offset, $value) {
		return self::_handleSet($name, $value);
	}
	public function offsetGet($offset) {
		return self::_handleGet($name);
	}
	public function offsetExists($offset) {
		self::_load();
		return array_key_exists(trim(strtolower($key)), self::$_fields);
    }
	public function offsetUnset($offset) {
		//will do nothing as we don't want to unset
    }
}