<?php
include_once( "composer_autoloader.php" );
//include dotenv var
include_once( "dotenv_autoloader.php" );
return [
  'paths' => [
    'migrations' => 'vendor/jklz/settings-mgr/src/migration'
  ],
  'migration_base_class' => '\jklz_settings_mgr\Migration\Migration',
  'environments' => [
    'default_migration_table' => 'settingsmgr_migration',
    'default_database' => 'settingsmgr',
    'settingsmgr' => [
      'adapter' => $_ENV['DB_DRIVER'],
      'host' => $_ENV['DB_HOST'],
      'name' => $_ENV['DB_DATABASE'],
      'user' => $_ENV['DB_USERNAME'],
      'pass' => $_ENV['DB_PASSWORD'],
      'port' => $_ENV['DB_PORT']
    ]
  ]
];