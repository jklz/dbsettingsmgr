<?php
return function () {
    $locations = [
      __DIR__ . '/../../../autoload.php',
      __DIR__ . '/../vendor/autoload.php',
    ];
    foreach ($locations as $file) {
        if (file_exists($file)) {
            require_once $file;
            return true;
        }
    }
    return false;
};
