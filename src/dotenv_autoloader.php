<?php 
include_once( "composer_autoloader.php" );
//locations our .env may be located
$env_locs = [
	__DIR__ . '/../../../../app/',
	__DIR__ . '/../app/',
	__DIR__ . '/../../../../',
	__DIR__ . '/../'
];
$env_loc = NULL;
foreach ($env_locs as $loc) {
	if (file_exists($loc.".env")) {
		$env_loc =  $loc;
		break;
	}
}
$dotenv = new \Dotenv\Dotenv($env_loc);
$dotenv->load();